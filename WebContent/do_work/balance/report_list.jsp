<%@page import="java.util.TreeSet"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>修改資料</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"
	charset="UTF-8">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/carousel.css" rel="stylesheet">

<%
	HashMap<String, Double> changed_balance_expected = (HashMap<String, Double>) session
			.getAttribute("changed_balance_expected"); ////資料有變動的帳戶預期餘額
	HashMap<String, Double> changed_balance_real = (HashMap<String, Double>) session
			.getAttribute("changed_balance_real"); ////資料有變動的帳戶實際餘額
	String[] changed_ids = (String[]) session.getAttribute("changed_ids");
%>

<head>

<body style="background-color: #eaedea;">

	<table class="table table-bordered text-center ">
		<thead>
			<tr>
				<th scope="col" width="30%"><small>Trading Account ID</small></th>
				<th scope="col" width="10%"><small>預期餘額</small></th>
				<th scope="col" width="10%"><small>實際餘額</small></th>

			</tr>
		</thead>
		<tbody>
			<%
				for (String x : changed_ids) {
			%>
			<tr>
				<td class="align-middle"><small><%=x %></small></td>
				<td class="align-middle"><small><%=changed_balance_expected.get(x) %></small></td>
				<td class="align-middle"><small><%=changed_balance_real.get(x) %></small></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>
	
</body>

</html>