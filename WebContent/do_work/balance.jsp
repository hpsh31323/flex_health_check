<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>

<!-- 店家網站首頁,店家資訊也會連到這邊   -->
<html>

    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
 <%@ include file="head.jsp"%>
  </head>
   
<body style="background-color:#eff9ff;">
  <div class="container marketin" style="margin-top:6rem;">
<div class="row justify-content-md-center" >
  <div class="col-3 ">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active text-center" id="list-result_head-list" data-toggle="list" href="#list-result_head" role="tab" aria-controls="result_head">檢查結果</a>
      <a class="list-group-item list-group-item-action text-center" id="list-report_list-list" data-toggle="list" href="#list-report_list" role="tab" aria-controls="report_list">帳戶變動清單</a>
   <!--  
      <a class="list-group-item list-group-item-action text-center"        id="list-promotion-list" data-toggle="list" href="#list-promotion" role="tab" aria-controls="promotion">促銷統計</a>
      <a class="list-group-item list-group-item-action text-center" id="list-period-list" data-toggle="list" href="#list-period" role="tab" aria-controls="period">時段比較</a>
      -->
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-result_head" role="tabpanel" aria-labelledby="list-result_head-list"><%@ include file="balance/result_head.jsp"%></div>
      <div class="tab-pane fade show " id="list-report_list" role="tabpanel" aria-labelledby="list-report_list-list"><%@ include file="balance/report_list.jsp"%></div>
      <!--  
      <div class="tab-pane fade" id="list-promotion" role="tabpanel" aria-labelledby="list-promotion-list">,./,./,./,./,./,./,./,./.,/,./,./,./</div>
      <div class="tab-pane fade" id="list-period" role="tabpanel" aria-labelledby="list-period-list">452345234tert245tg4eryt245yt</div>
   -->
    </div>
  </div>
</div>
</div>
   </body>
</html>