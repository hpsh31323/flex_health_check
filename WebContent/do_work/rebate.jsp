<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html>

<!-- 店家網站首頁,店家資訊也會連到這邊   -->
<html>

    <title>MainPage</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"  charset="UTF-8">
      <link href="../css/bootstrap.min.css" rel="stylesheet">
      <link href="../css/carousel.css" rel="stylesheet">
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="../js/bootstrap.min.js"></script>
             
   <head>
 <%@ include file="head.jsp"%>
  </head>
   
<body style="background-color:#eff9ff;">
  <div class="container marketin" style="margin-top:6rem;">
<div class="row justify-content-md-center" >
  <div class="col-3 ">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active text-center" id="list-rebate_reconciliation_tickets-list" data-toggle="list" href="#list-rebate_reconciliation_tickets" role="tab" aria-controls="rebate_reconciliation_tickets">BY Tickets ID</a>
		<a class="list-group-item list-group-item-action  text-center" id="list-rebate_reconciliation_user-list" data-toggle="list" href="#list-rebate_reconciliation_user" role="tab" aria-controls="rebate_reconciliation_user">BY USER ID</a>
   <!--  
      <a class="list-group-item list-group-item-action text-center"        id="list-promotion-list" data-toggle="list" href="#list-promotion" role="tab" aria-controls="promotion">促銷統計</a>
      <a class="list-group-item list-group-item-action text-center" id="list-period-list" data-toggle="list" href="#list-period" role="tab" aria-controls="period">時段比較</a>
      -->
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-rebate_reconciliation_tickets" role="tabpanel" aria-labelledby="list-rebate_reconciliation_tickets-list"><%@ include file="rebate/rebate_reconciliation_tickets.jsp"%></div>
		<div class="tab-pane fade show" id="list-rebate_reconciliation_user" role="tabpanel" aria-labelledby="list-rebate_reconciliation_user-list"><%@ include file="rebate/rebate_reconciliation_user.jsp"%></div>

      <!--  
      <div class="tab-pane fade" id="list-promotion" role="tabpanel" aria-labelledby="list-promotion-list">,./,./,./,./,./,./,./,./.,/,./,./,./</div>
      <div class="tab-pane fade" id="list-period" role="tabpanel" aria-labelledby="list-period-list">452345234tert245tg4eryt245yt</div>
   -->
    </div>
  </div>
</div>
</div>
   </body>
</html>