<%@page import="java.util.HashMap"%>
<%@page import="java.util.TreeSet"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<%
HashMap<String, String> lost_tickets = (HashMap<String, String>)session.getAttribute("lost_tickets"); ////無返佣紀錄的tickets
String[] lost_ticket = (String[])session.getAttribute("lost_ticket");
%>


<body style="background-color: #ecedea;">
	<div class="container marketing">
		<div class="py-4 text-center">
			<h5>
				<a class="text-muted"> 執行工作</a>
			</h5>
			<form action="../do_some_work" method="post">
			<table class="table text-center ">
				<thead>
					<tr>
						<th scope="col">
							<div class="form-row justify-content-center">
								<label>今天日期： <%= new SimpleDateFormat( "yyyy-MM-dd" ).format(Calendar.getInstance().getTime()) %></label> 
							</div>
						</th>
						<th scope="col">
						      <!--  客戶對帳 -->
		<div class="custom-control custom-radio">
			<div class="row justify-content-center">
		  <input type="radio" class="custom-control-input" id="data_retrieval" name="do_work" value = "1" >
		  <label class="custom-control-label" for="data_retrieval"> 客戶對帳</label>
		  </div>
		</div>
		
	<!-- 返佣對賬-->
		<div class="custom-control custom-radio">
					<div class="row justify-content-center">
		  <input type="radio" class="custom-control-input" id="rebate_reconciliation" name="do_work" value = "2" checked>
		  <label class="custom-control-label" for="rebate_reconciliation"> 返佣對賬</label>
		</div>
		</div>
						</th>
						<th scope="col" class="text-right"><button type="submit"
								class="btn btn-primary">執行</button></th>
					</tr>
				</thead>
			</table>
			</form>
		</div>

		<br>

		<div class="row">

			<div class="col-lg-12">
				<div class="card border-info mb-3">
					<div class="card-body text-dark">
						<div class="row">

							<div class="col-lg-12">
					<div>返佣比對結果： <%=(String)session.getAttribute("note") %></div>

							</div>
							</div>
					</div>
				</div>
			</div>
		</div>

	<table class="table table-bordered text-center ">
		<thead>
			<tr>
				<th scope="col" width="65%"><small>LOST TICKET ID</small></th>
				<th scope="col" width="35%"><small>USER ID</small></th>

			</tr>
		</thead>
		<tbody>
			<%
				for (String x : lost_ticket) {
			%>
			<tr>
				<td class="align-middle"><small><%=x %></small></td>
				<td class="align-middle"><small><%=lost_tickets.get(x) %></small></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>


<!--  
		<table class="table table-bordered  table-striped text-center ">
			<thead class="thead-dark">
				<tr>
					<th scope="col"><small>訂單時間</small></th>
					<th scope="col"><small>訂單編號</small></th>
					<th scope="col"><small>訂單類別</small></th>
					
					
					<th scope="col"><small>訂單金額</small></th>
					<th scope="col"><small>首購客戶</small></th>
					
					<th scope="col"><small>訂單狀態</small></th>
					
					<th scope="col" width="20%"><small>訂單註記</small></th>


				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>

				</tr>


				<tr>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>
					<td class="align-middle"></td>

				</tr>
			</tbody>
		</table>
-->
	</div>

</body>
</html>