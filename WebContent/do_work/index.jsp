<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<title>MainPage</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0 , shrink-to-fit=no"
	charset="UTF-8">

<link href="../css/floating-labels.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	crossorigin="anonymous">
<link href="../css/fileinput.css" media="all" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
	crossorigin="anonymous">
<link href="../themes/explorer-fas/theme.css" media="all"
	rel="stylesheet" type="text/css" />
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"
	crossorigin="anonymous"></script>
<script src="../js/plugins/piexif.js" type="text/javascript"></script>
<script src="../js/plugins/sortable.js" type="text/javascript"></script>
<script src="../js/fileinput.js" type="text/javascript"></script>
<script src="../js/locales/fr.js" type="text/javascript"></script>
<script src="../js/locales/es.js" type="text/javascript"></script>
<script src="../themes/fas/theme.js" type="text/javascript"></script>
<script src="../themes/explorer-fas/theme.js" type="text/javascript"></script>
<script src="../js/locales/zh-TW.js" type="text/javascript"></script>

<head>
<meta charset="UTF-8">
<%@ include file="head.jsp"%>
</head>


<body style="background-color: #eff9ff;">
	<form class="form-signin" method="post" action="../do_some_work">
		<div class="text-center mb-4">
			<img class="mb-4" src="../img/fundroots_logo.png" alt="" width="150">
			<h1 class="h3 mb-3 font-weight-normal">FLEX HEALTHCHECK</h1>
		</div>

		<table class="table text-center ">
			<thead>
				<tr>
					<th scope="col">
						<div class="row justify-content-center">
							<label>今天日期： <%=new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime())%></label>
						</div>
					</th>
			</thead>
			<tbody>
				<tr>
					<th scope="col">
						<div class="row justify-content-center">
							<button type="button" class="btn btn-primary" data-toggle="modal"
								data-target="#upload_data">上傳檔案</button>
						</div>
					</th>
			</tbody>
		</table>
		<!--  客戶對帳 -->
		<div class="custom-control custom-radio">
			<div class="row justify-content-center">
				<input type="radio" class="custom-control-input" id="data_retrieval"
					name="do_work" value="1" checked> <label
					class="custom-control-label" for="data_retrieval"> 客戶對帳</label>
			</div>
		</div>

		<!-- 返佣對賬-->
		<div class="custom-control custom-radio">
			<div class="row justify-content-center">
				<input type="radio" class="custom-control-input"
					id="rebate_reconciliation" name="do_work" value="2"> <label
					class="custom-control-label" for="rebate_reconciliation">
					返佣對賬</label>
			</div>
		</div>
		<br>
		<button class="btn btn-lg btn-primary btn-block" type="submit">執行</button>

	</form>

	<!-- 以下為Dialog-->
	<!-- Modal -->
	<div class="modal fade" id="upload_data" tabindex="-1" role="dialog"
		aria-labelledby="upload_data" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="upload_file">上傳檔案(施工中...)</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form enctype="multipart/form-data" >
						<div class="file-loading">
							<input id="file" class="file" type="file" multiple
								data-min-file-count="1" data-theme="fas">
						</div>
						<br>
						<div class="custom-control custom-radio">
							<div class="row justify-content-center">
								<div class="col-md-3">
									<input type="radio" class="custom-control-input" id="upload1"
										name="upload" value="1" checked> <label
										class="custom-control-label" for="upload1">昨日餘額</label>
								</div>
								<div class="col-md-3">
									<input type="radio" class="custom-control-input" id="upload2"
										name="upload" value="2"> <label
										class="custom-control-label" for="upload2"> 今日餘額</label>
								</div>
								<div class="col-md-3">
									<input type="radio" class="custom-control-input" id="upload3"
										name="upload" value="3"> <label
										class="custom-control-label" for="upload3"> 資金調整</label>
								</div>
								<div class="col-md-3">
									<input type="radio" class="custom-control-input" id="upload4"
										name="upload" value="4"> <label
										class="custom-control-label" for="upload4"> 返佣紀錄</label>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-md-4">
									<input type="radio" class="custom-control-input" id="upload5"
										name="upload" value="5"> <label
										class="custom-control-label" for="upload5">出入金紀錄</label>
								</div>
								<div class="col-md-4">
									<input type="radio" class="custom-control-input" id="upload6"
										name="upload" value="6"> <label
										class="custom-control-label" for="upload6">已結算交易</label>
								</div>
								<div class="col-md-4">
									<input type="radio" class="custom-control-input" id="upload7"
										name="upload" value="7"> <label
										class="custom-control-label" for="upload7">進行中交易</label>
								</div>
							</div>
						</div>
						<br>
						<button type="submit" class="btn btn-primary" disabled>確定上傳</button>
						<button type="reset" class="btn btn-outline-secondary">Reset</button>
					</form>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">取消</button>
				</div>
			</div>
		</div>
	</div>
</body>

<!--初始化fileinput控制元件（第一次初始化）  -->
<script>
	$("#file").fileinput({
			language : 'zh-TW', //設定語言
			allowedFileExtensions : [ 'csv', 'sql' ],//接收的檔案字尾
			showUpload : false, //是否顯示上傳按鈕
			showCaption : false,//是否顯示標題
			browseClass : "btn btn-primary", //按鈕樣式 
			previewFileIcon : "<i class='glyphicon glyphicon-king'></i>",
	        uploadExtraData:function(){  //向後台傳輸參數
	            var data={
	                filetype:$("#upload").val(), //取得id為upload的val值
	            };
	            return data;
	            }
		});
	
</script>

</html>