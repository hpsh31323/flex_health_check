package healthcheck_servelt;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import healthcheck.data_retrieval;
import healthcheck.rebate_reconciliation;

/**
 * Servlet implementation class do_some_work
 */
@WebServlet("/do_some_work")
public class do_some_work extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public do_some_work() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		HttpSession session=request.getSession();
		PrintWriter out = response.getWriter();

		int do_work = Integer.parseInt(request.getParameter("do_work"));
		HashMap<String, Object> result = null;

		try {
			
		switch (do_work) {
		case 1:
			result = data_retrieval.check();
			HashMap<String, Double> changed_balance_expected = (HashMap<String, Double>) result.get("changed_balance_expected"); ////資料有變動的帳戶預期餘額
			HashMap<String, Double> changed_balance_real = (HashMap<String, Double>) result.get("changed_balance_real"); ////資料有變動的帳戶實際餘額
			TreeSet<String> problem_data_id = (TreeSet<String>) result.get("problem_data_id"); //// 帳目不符的account id 
			String [] changed_ids = new String[changed_balance_expected.keySet().size()];
			changed_balance_expected.keySet().toArray(changed_ids);
			
			session.setAttribute("changed_balance_expected", changed_balance_expected );
			session.setAttribute("changed_balance_real", changed_balance_real );
			session.setAttribute("changed_balance_expected2", changed_balance_expected );
			session.setAttribute("changed_balance_real2", changed_balance_real );
			session.setAttribute("problem_data_id", problem_data_id );
			session.setAttribute("changed_ids", changed_ids );
			if(problem_data_id.size()==0) {
				session.setAttribute("note", "帳目無異常" );
			}else {
				session.setAttribute("note", "帳目異常" );
			}
			
			response.sendRedirect("do_work/balance.jsp");
					
			break;

		case 2:
			result = rebate_reconciliation.check();
			HashMap<String, String> lost_tickets = (HashMap<String, String>) result.get("lost_tickets"); ////無返佣紀錄的tickets
			HashMap<String,String> userids = (HashMap<String, String>) result.get("userids"); ////有無返佣紀錄ticket的user
			TreeSet<String> useridSet = (TreeSet<String>) result.get("useridSet");
			String [] lost_ticket = (String[]) result.get("lost_ticket");
			String note = (String) result.get("note");
			session.setAttribute("lost_ticket", lost_ticket);
			session.setAttribute("lost_tickets", lost_tickets);
			session.setAttribute("useridSet", useridSet);
			session.setAttribute("userids", userids);
			session.setAttribute("note", note);
			response.sendRedirect("do_work/rebate.jsp");
			
			break;
		}
			
		} catch (ClassNotFoundException | SQLException e) {

			e.printStackTrace();
		}
		
	}

}
