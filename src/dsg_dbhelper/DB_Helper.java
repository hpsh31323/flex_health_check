package dsg_dbhelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeSet;

import dsg_dbhelper.Arith;;

public class DB_Helper {
	
	private String url = "jdbc:mysql://localhost:3306/healthcheck?useSSL=true&usrUnicode=true&characterEncoding=utf-8";
	private String user = "root";
	private String password = "p@ssw0rd";
	protected Connection con;
	/*
	public static void main(String[] args) {
		
			try {
				DB_Helper db = new DB_Helper();
				
				HashMap<String, Double> accountbalance_lastday = db.get_all_accountbalances_lastday();
				HashMap<String, Double> accountbalance_today = db.get_all_accountbalances_today();
				
				String [] id_lastday = new String [accountbalance_lastday.keySet().size()];
				String [] id_today = new String [accountbalance_today.keySet().size()];
				
				accountbalance_lastday.keySet().toArray(id_lastday);
				accountbalance_today.keySet().toArray(id_today);
				
				System.out.println("帳號\t\t\t昨日餘額\t\t\t今日");
				for(String x : id_lastday) {
					System.out.println(x+"\t"+accountbalance_lastday.get(x)+"\t"+accountbalance_today.get(x));
				}
				
				System.out.println("============================================");
				
				System.out.println("success ");
			} catch (ClassNotFoundException e) {
				System.out.println("class not found");
				e.printStackTrace();
			} catch (SQLException e) {
				System.out.println("SQL ERROR");
				e.printStackTrace();
			}
	}
	*/
	public DB_Helper() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection(url, user,password );
		
	}
	
	public Connection dsg_connect() {
		return this.con;
	}
	
	////取出closed_trades data 篩選後get profit
	public HashMap<String, Double> get_closed_trades_profit(String date1,String date2) throws SQLException {
		
		HashMap<String, Double> close_trades_profit = new HashMap<String, Double>();
	
		String sql = String.format("SELECT trading_account_id,profit\r\n" + 
				"FROM healthcheck.closed_trades\r\n" + 
				"where is_point_trade = 'FALSE' && agent_name !='Top IB Testing' \r\n" + 
				"AND close_dateTime > '" + date1 + " 06:01:00' " + 
				"AND close_dateTime < '" + date2 + " 05:59:00' " + 
				"ORDER BY trading_account_id ASC;");
		String accountid = null;
		double profit = 0.0;
		
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while(rs.next()){
		
			accountid = rs.getString("trading_account_id");
			//System.out.print(accountid+"\t");
			profit = rs.getDouble("profit");
			//System.out.print("profit record ="+profit+"\t");
			
			if (close_trades_profit.containsKey(accountid)) {
				
				profit = Arith.add(profit, close_trades_profit.get(accountid));
				//System.out.print("profit add ="+profit+"\t");
				profit = Arith.round(profit,2);
				//System.out.println("profit add round ="+profit+"\t");
				close_trades_profit.put(accountid, profit);
			}else{
				
				close_trades_profit.put(accountid, profit);
				//System.out.println();
			}
		}
		rs.close();
		st.close();
		return close_trades_profit;
	}
	
	////取出deposit_withdrawal_records data 篩選後get amount_usd
	public HashMap<String, Double> get_deposit_withdrawal_records(String date1,String date2) throws SQLException {
		
		HashMap<String, Double> deposit_withdrawal_records = new HashMap<String, Double>();
		
		String sql = String.format("SELECT trading_account_id,amount_usd " + 
				"FROM healthcheck.deposit_withdrawal_records " + 
				"where parent_agent_name !='Top IB Testing' && approval_datetime != '' "+
				"AND approval_datetime > '" + date1 + " 06:01:00' " + 
				"AND approval_datetime < '" + date2 + " 05:59:00';");
		
		String accountid = null;
		double amount_usd = 0.0;
		
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while(rs.next()){
			
			accountid = rs.getString("trading_account_id");
			amount_usd = rs.getDouble("amount_usd");
			
			if (deposit_withdrawal_records.containsKey(accountid)) {
				
				amount_usd = Arith.add(amount_usd, deposit_withdrawal_records.get(accountid));
				deposit_withdrawal_records.put(accountid, amount_usd);
				
			}else{
				
				deposit_withdrawal_records.put(accountid, amount_usd);
			}
		}
		rs.close();
		st.close();
		return deposit_withdrawal_records;
	}
	
	////取出balance_adjustment_records data 篩選後get amount_usd & agent's rebate
	public HashMap<String, Double> get_balance_adjustment_records(String date1,String date2) throws SQLException, ClassNotFoundException {
		
		HashMap<String, Double> balance_adjustment_records = new HashMap<String, Double>();
		TreeSet<String> accountidSet = new TreeSet<String>();
		
		String sql = String.format("SELECT trading_account_id,amount_usd,trading_account_type " + 
				"FROM healthcheck.balance_adjustment_records " + 
				"where top_level_agent_name != 'Top IB Testing';");
		String accountid = null;
		String account_type = null;
		double amount_usd = 0.0;
		
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while(rs.next()){
			
			accountid = rs.getString("trading_account_id");
			amount_usd = rs.getDouble("amount_usd");
			account_type = rs.getString("trading_account_type");
			
			if (balance_adjustment_records.containsKey(accountid)) {
				
				amount_usd = Arith.add(amount_usd, balance_adjustment_records.get(accountid));				
				balance_adjustment_records.put(accountid, amount_usd);
				
			}else{
				
				balance_adjustment_records.put(accountid, amount_usd);
			}
			
			if(account_type.equals("AGENT")) {
				accountidSet.add(accountid);
			}
			
		}
		rs.close();

		
		////取出rebate_records 計算指定  accountID  的 rebate總和  
		
			if(!accountidSet.isEmpty()) {
				
				for(String x:accountidSet) {
					
				double rebate = 0.0;

				String sql2 = "SELECT rebate_amount_usd " + 
						"FROM healthcheck.rebate_records " + 
						"where trading_account_id = '"+ x +"' " + 
						"AND creation_dateTime > '" + date1 + " 06:01:00' " + 
						"AND creation_dateTime < '" + date2 + " 05:59:00';";
				
				ResultSet rs2 = st.executeQuery(sql2);
				
				while(rs2.next()) {
					rebate += rs2.getDouble("rebate_amount_usd");
				}
				rs2.close();
				st.close();
				
				amount_usd = Arith.add(rebate, balance_adjustment_records.get(x));				
				balance_adjustment_records.put(x, amount_usd);
				
				}
			}
			//////////////////////////////////////////////
		
		
		return balance_adjustment_records;
	}
	
	////取出changed data's  lastday accountbalances 
	public HashMap<String, Double> get_accountbalances_lastday(String [] s) throws SQLException {
		
		HashMap<String, Double> accountbalances_lastday = new HashMap<String, Double>();
		
		String x = "";
		for(int i = 0 ; i < s.length ; i++) {
			
			x += "accountId = '"+ s[i]+"'";
			
			if(i != s.length-1) {
				x += " or ";
			}
		}
		
		
		String sql = String.format("SELECT accountId,balance " + 
				"FROM healthcheck.accountbalances " + 
				"where currency = 'USD' and (" + x + ") ;");
		
		String accountid = null;

		double div_value = 10000000000.0 ; 
		double balance = 0.0;	
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while(rs.next()){
			
			accountid = rs.getString("accountId");
			balance = Arith.div(rs.getDouble("balance"), div_value, 2);
			
			if (accountbalances_lastday.containsKey(accountid)) {
				
				balance = Arith.add(balance, accountbalances_lastday.get(accountid));
				accountbalances_lastday.put(accountid, balance);
				
			}else{
				
				accountbalances_lastday.put(accountid, balance);
			}
		}
		rs.close();
		st.close();
		
		return accountbalances_lastday;
	}
	
	////取出changed data's  today accountbalances 
	public HashMap<String, Double> get_accountbalances_today(String [] s) throws SQLException {
		
		HashMap<String, Double> accountbalances_today = new HashMap<String, Double>();
		
		String x = "";
		for(int i = 0 ; i < s.length ; i++) {
			
			x += "accountId = '"+ s[i]+"'";
			
			if(i != s.length-1) {
				x += " or ";
			}
		}
		
		
		String sql = String.format("SELECT accountId,balance " + 
				"FROM healthcheck.accountbalances_today " + 
				"where currency = 'USD' and (" + x + ") ;");
		
		String accountid = null;

		double div_value = 10000000000.0 ; 
		double balance = 0.0;	
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while(rs.next()){
			
			accountid = rs.getString("accountId");
			balance = Arith.div(rs.getDouble("balance"), div_value, 2);
			
			if (accountbalances_today.containsKey(accountid)) {
				
				balance = Arith.add(balance, accountbalances_today.get(accountid));
				accountbalances_today.put(accountid, balance);
				
			}else{
			
				accountbalances_today.put(accountid, balance);
			}
		}
		rs.close();
		st.close();
		
		return accountbalances_today;
	}
	
	////取出closed_trades 篩選後get TICKET ID & user_id
	
	public HashMap<String,String> get_closed_trades_ticketids(String date1,String date2) throws SQLException{
		
		HashMap<String,String>tickets = new HashMap<String,String>();
			
		String sql = String.format("SELECT ticket_id,user_id FROM healthcheck.closed_trades " + 
				"where is_point_trade = 'FALSE' " + 
				"AND create_dateTime > '" + date1 + " 06:01:00' " + 
				"AND create_dateTime < '" + date2 + " 05:59:00' " + 
				"AND agent_name !='Top IB Testing' "+
				"AND (qtyOz != 0 OR qtyEUR != 0 OR qtyGBP != 0 OR qtyBBL != 0 OR qtyUSD);");
		
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()) {
			tickets.put(rs.getString("ticket_id"),rs.getString("user_id"));
		}
		
		return tickets;
	}
	
	////取出opening_trades 篩選後get TICKET ID & user_id
	
	public HashMap<String,String> get_opening_trades_ticketids(String date1,String date2) throws SQLException{
		
		HashMap<String,String> tickets = new HashMap<String,String>();
				
		String sql = String.format("SELECT ticket_id,user_id FROM healthcheck.opening_trades " + 
				"where is_point_trade = 'FALSE' " + 
				"AND create_dateTime > '" + date1 + " 06:01:00' " + 
				"AND create_dateTime < '" + date2 + " 05:59:00' " + 
				"AND agent_name !='Top IB Testing' "+
				"AND (qtyOz != 0 OR qtyEUR != 0 OR qtyGBP != 0 OR qtyBBL != 0 OR qtyUSD);");
		
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()) {
			tickets.put(rs.getString("ticket_id"),rs.getString("user_id"));
		}
		
		return tickets;
	}
	
	////取出rebate_records 篩選後get TICKET ID  
	
	public ArrayList<String> get_rebate_records_ticketids() throws SQLException{
		
		ArrayList<String> tickets = new ArrayList<String>();
		
		String sql = "SELECT ticket_id FROM healthcheck.rebate_records;";
		
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		
		while(rs.next()) {
			tickets.add(rs.getString("ticket_id"));
		}
		
		return tickets;
	}
	
	////取出ALL  today accountbalances 
	public HashMap<String, Double> get_all_accountbalances_today() throws SQLException {
		
		HashMap<String, Double> accountbalances_today = new HashMap<String, Double>();
		
		String sql = String.format("SELECT accountId,balance " + 
				"FROM healthcheck.accountbalances_today " + 
				"where currency = 'USD';");
		
		String accountid = null;

		double div_value = 10000000000.0 ; 
		double balance = 0.0;	
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while(rs.next()){
			
			accountid = rs.getString("accountId");
			balance = Arith.div(rs.getDouble("balance"), div_value, 2);		
			
			accountbalances_today.put(accountid, balance);
			
		}
		rs.close();
		st.close();
		
		return accountbalances_today;
	}

	
	////取出ALL  lastday accountbalances 
	public HashMap<String, Double> get_all_accountbalances_lastday() throws SQLException {
		
		HashMap<String, Double> accountbalances_lastday = new HashMap<String, Double>();
				
		String sql = String.format("SELECT accountId,balance " + 
				"FROM healthcheck.accountbalances " + 
				"where currency = 'USD';");
		
		String accountid = null;

		double div_value = 10000000000.0 ; 
		double balance = 0.0;	
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);
		while(rs.next()){
			
			accountid = rs.getString("accountId");
			balance = Arith.div(rs.getDouble("balance"), div_value, 2);
				
			accountbalances_lastday.put(accountid, balance);
			
		}
		rs.close();
		st.close();
		
		return accountbalances_lastday;
	}
}
