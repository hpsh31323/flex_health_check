package healthcheck;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeSet;

import dsg_dbhelper.Arith;
import dsg_dbhelper.DB_Helper;

public class data_retrieval {
	
	/*
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		HashMap<String, Double> changed_balance_expected = new HashMap<String, Double>(); ////資料有變動的帳戶預期餘額
		HashMap<String, Double> changed_balance_real = new HashMap<String, Double>(); ////資料有變動的帳戶實際餘額
		TreeSet<String> problem_data_id = new TreeSet<String>();   //// 帳目不符的account id
				try {
					result = data_retrieval.check();
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		changed_balance_expected = (HashMap<String, Double>) result.get("changed_balance_expected");
		changed_balance_real = (HashMap<String, Double>) result.get("changed_balance_real");
		problem_data_id = (TreeSet<String>) result.get("problem_data_id");
				
		System.out.print("test");
	}
	*/
	private data_retrieval() {
	}
	
	public static HashMap<String, Object> check() throws SQLException, ClassNotFoundException {
		
		 HashMap<String, Double> changed_data = new HashMap<String, Double>(); ////資料有變動的帳戶
		 HashMap<String, Double> changed_balance_expected = new HashMap<String, Double>(); ////資料有變動的帳戶預期餘額
		 HashMap<String, Double> changed_balance_real = new HashMap<String, Double>(); ////資料有變動的帳戶實際餘額
		 HashMap<String, Double> accountbalances_lastday = new HashMap<String, Double>();
		 HashMap<String, Double> accountbalances_today = new HashMap<String, Double>();
		 HashMap<String, Double> close_trade = new HashMap<String, Double>();
		 HashMap<String, Double> deposit_withdrawal = new HashMap<String, Double>();
		 HashMap<String, Double> balance_adjustment = new HashMap<String, Double>();
		 TreeSet<String> problem_data_id = new TreeSet<String>();   //// 帳目不符的account id
		 HashMap<String, Object> result = new HashMap<String, Object>();
		 
		 
		 DB_Helper db = new DB_Helper();
		 
		 Calendar calendar = Calendar.getInstance();
		 SimpleDateFormat dateStringFormat = new SimpleDateFormat( "yyyy-MM-dd" );
			
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH)+1;
			int day = calendar.get(Calendar.DAY_OF_MONTH);		
			int week = calendar.get(calendar.DAY_OF_WEEK);
			String date1 = null;
			String date2 = dateStringFormat.format(calendar.getTime());
			
			//////////////////////
			switch (week) {
			case 2:
				date1 = String.format("%04d-%02d-%02d", year,month,day-3);
				break;

			case 7:
			case 3:
			case 4:
			case 5:
			case 6:
				date1 = String.format("%04d-%02d-%02d", year,month,day-1);
				break;
				
			case 1:
				date1 = String.format("%04d-%02d-%02d", year,month,day-2);
				break;
			}
		    
			
		
		
			//////取出變動資料
			String keyString = null;
			double value = 0.0;
			close_trade = db.get_closed_trades_profit(date1,date2);
			deposit_withdrawal = db.get_deposit_withdrawal_records(date1,date2);
			balance_adjustment = db.get_balance_adjustment_records(date1,date2);
			
			String [] accountidsSet1 = new String [close_trade.keySet().size()];
			String [] accountidsSet2 = new String [deposit_withdrawal.keySet().size()];
			String [] accountidsSet3 = new String [balance_adjustment.keySet().size()];
			
			close_trade.keySet().toArray(accountidsSet1);
			deposit_withdrawal.keySet().toArray(accountidsSet2);
			balance_adjustment.keySet().toArray(accountidsSet3);
			
			////changed_data 加入上一日交易金額
			for(int x = 0 ; x < accountidsSet1.length ; x++) {
				
				keyString = accountidsSet1[x];
				value = close_trade.get(accountidsSet1[x]);
				changed_data.put(keyString, value);
			}
			
			////changed_data 加入上一日出入金金額
			for(int x = 0 ; x < accountidsSet2.length ; x++) {
				
				keyString = accountidsSet2[x];
				value = deposit_withdrawal.get(accountidsSet2[x]);

				if(changed_data.containsKey(keyString)) {

					value = Arith.add(value, changed_data.get(keyString)) ;
					changed_data.put(keyString, value);

				}else {
					changed_data.put(keyString, value);
				}
				
			}
			
			////changed_data 加入上一日調整金額
			for(int x = 0 ; x < accountidsSet3.length ; x++) {
				
				keyString = accountidsSet3[x];
				value = balance_adjustment.get(accountidsSet3[x]);

				if(changed_data.containsKey(keyString)) {

					value = Arith.add(value, changed_data.get(keyString));
					changed_data.put(keyString, value);

				}else {
					changed_data.put(keyString, value);
				}
				 
			}
			

			String [] accountids = new String[changed_data.keySet().size()];
			changed_data.keySet().toArray(accountids);
			
		////取得accountbalances_lastday
			accountbalances_lastday = db.get_accountbalances_lastday(accountids);	//取指定
			
		////取得accountbalances_today
			accountbalances_today = db.get_accountbalances_today(accountids);   //取指定
					
		
		
			
		////對帳
		for(int i = 0 ; i < accountids.length ; i++) {
			if(accountbalances_lastday.get(accountids[i])!=null) {     ///非前日新增帳戶
				
				double value1 = Arith.add(changed_data.get(accountids[i]), accountbalances_lastday.get(accountids[i]));
				double value2 = accountbalances_today.get(accountids[i]);
				
				if(value1 != value2) {
					problem_data_id.add(accountids[i]);
				}
				
			}else {                                                       ///前日新增帳戶
				
				double value1 = changed_data.get(accountids[i]);
				double value2 = accountbalances_today.get(accountids[i]);
				if(value1 != value2) {
					problem_data_id.add(accountids[i]);
				}
			}
			
		}
		
		result.put("problem_data_id", problem_data_id);  // 放入有問題的ID資料
		
		for(String x : accountids) {
			
			changed_balance_expected.put(x, Arith.add(changed_data.get(x), (accountbalances_lastday.get(x)!=null)?accountbalances_lastday.get(x):0));
			changed_balance_real.put(x, accountbalances_today.get(x));
		}
		result.put("changed_balance_expected", changed_balance_expected); // 放入預期餘額
		result.put("changed_balance_real", changed_balance_real); // 放入實際餘額
		
		
		return result;
		
	}
	/*
	public static void checkall() {
		
		 HashMap<String, Double> changed_data = new HashMap<String, Double>();
		 HashMap<String, Double> accountbalances_lastday_all = new HashMap<String, Double>();
		 HashMap<String, Double> accountbalances_today_all = new HashMap<String, Double>();
		 
		 Calendar calendar = Calendar.getInstance();
		 SimpleDateFormat dateStringFormat = new SimpleDateFormat( "yyyy-MM-dd" );
			
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH)+1;
			int day = calendar.get(Calendar.DAY_OF_MONTH);		
			int week = calendar.get(calendar.DAY_OF_WEEK);
			String date1 = null;
			String date2 = dateStringFormat.format(calendar.getTime());
			
			//////////////////////
			switch (week) {
			case 2:
				date1 = String.format("%04d-%02d-%02d", year,month,day-3);
				break;

			case 7:
			case 3:
			case 4:
			case 5:
			case 6:
				date1 = String.format("%04d-%02d-%02d", year,month,day-1);
				break;
				
			case 1:
				date1 = String.format("%04d-%02d-%02d", year,month,day-2);
				break;
			}
		    
		
		try {
			DB_Helper db = new DB_Helper();
			
			String keyString = null;
			double value = 0.0;
			
			HashMap<String, Double> close_trade = db.get_closed_trades_profit(date1,date2);
			HashMap<String, Double> deposit_withdrawal = db.get_deposit_withdrawal_records(date1,date2);
			HashMap<String, Double> balance_adjustment = db.get_balance_adjustment_records(date1,date2);
			
			String [] accountidsSet1 = new String [close_trade.keySet().size()];
			String [] accountidsSet2 = new String [deposit_withdrawal.keySet().size()];
			String [] accountidsSet3 = new String [balance_adjustment.keySet().size()];
			
			close_trade.keySet().toArray(accountidsSet1);
			deposit_withdrawal.keySet().toArray(accountidsSet2);
			balance_adjustment.keySet().toArray(accountidsSet3);
			
			////changed_data 加入上一日交易金額
			for(int x = 0 ; x < accountidsSet1.length ; x++) {
				
				keyString = accountidsSet1[x];
				value = close_trade.get(accountidsSet1[x]);
				//System.out.print(keyString+"\t");
				//System.out.print("close_trade value ="+value+"\t");
				changed_data.put(keyString, value);
				//System.out.println("changed_data value ="+changed_data.get(keyString));
			}
			
			////changed_data 加入上一日出入金金額
			for(int x = 0 ; x < accountidsSet2.length ; x++) {
				
				keyString = accountidsSet2[x];
				value = deposit_withdrawal.get(accountidsSet2[x]);
				//System.out.print(keyString+"\t");
				//System.out.print("deposit_withdrawal value ="+value+"\t");
				
				if(changed_data.containsKey(keyString)) {

					value = Arith.add(value, changed_data.get(keyString)) ;
					//System.out.print("deposit_withdrawal value add="+value+"\t");
					changed_data.put(keyString, value);
					//System.out.println("changed_data value ="+changed_data.get(keyString));
				}else {
					changed_data.put(keyString, value);
					//System.out.println("changed_data value ="+changed_data.get(keyString));
				}
				
			}
			
			////changed_data 加入上一日調整金額
			for(int x = 0 ; x < accountidsSet3.length ; x++) {
				
				keyString = accountidsSet3[x];
				value = balance_adjustment.get(accountidsSet3[x]);
				//System.out.print(keyString+"\t");
				//System.out.print("balance_adjustment value ="+value+"\t");
				
				if(changed_data.containsKey(keyString)) {

					value = Arith.add(value, changed_data.get(keyString));
					//System.out.print("deposit_withdrawal value add="+value+"\t");
					changed_data.put(keyString, value);
					//System.out.println("changed_data value ="+changed_data.get(keyString));
					
				}else {
					changed_data.put(keyString, value);
					//System.out.println("changed_data value ="+changed_data.get(keyString));
				}
				
			}
			

			String [] accountids = new String[changed_data.keySet().size()];
			changed_data.keySet().toArray(accountids);
			
		////取得accountbalances_lastday
			accountbalances_lastday_all = db.get_all_accountbalances_lastday(); //取全部
			String [] id_lastday = new String[accountbalances_lastday_all.keySet().size()];
			accountbalances_lastday_all.keySet().toArray(id_lastday);
			
		////取得accountbalances_today
			accountbalances_today_all = db.get_all_accountbalances_today();//取全部
			String [] id_today = new String[accountbalances_today_all.keySet().size()];
			accountbalances_today_all.keySet().toArray(id_today);
			

		} catch (ClassNotFoundException e) {
			System.out.println("class not found");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL ERROR");
			e.printStackTrace();
		}
		
		String result = null;
		TreeSet<String> problem_data_id = new TreeSet<String>();   //// 帳目不符的account id
		
		String [] accountids = new String [changed_data.keySet().size()];

		changed_data.keySet().toArray(accountids);
		
		////對帳
		for(int i = 0 ; i < accountids.length ; i++) {
			if(accountbalances_lastday_all.get(accountids[i])!=null) {     ///非前日新增帳戶
				
				double value1 = Arith.add(changed_data.get(accountids[i]), accountbalances_lastday_all.get(accountids[i]));
				double value2 = accountbalances_today_all.get(accountids[i]);
				
				if(value1 != value2) {
					problem_data_id.add(accountids[i]);
				}
				
			}else {                                                       ///前日新增帳戶
				
				double value1 = changed_data.get(accountids[i]);
				double value2 = accountbalances_today_all.get(accountids[i]);
				if(value1 != value2) {
					problem_data_id.add(accountids[i]);
				}
			}
			
		}
		
		if(problem_data_id.isEmpty()) {
			result = "ALL GREEN\n資料變動帳戶:\n";
			for(String x : accountids) {
				
				result += ""+ x +"\t預期餘額:"+ Arith.add(changed_data.get(x), accountbalances_lastday_all.get(x)) + 
						"\t實際餘額: " + accountbalances_today_all.get(x) +"\n";
			}
		}else {
			result = " We Got Problems\nProblem ID :\n";
			
			for (String x : problem_data_id) {
				
				result += ""+ x +"\t預期餘額:"+ Arith.add(changed_data.get(x), accountbalances_lastday_all.get(x)) + 
						"\t實際餘額: " + accountbalances_today_all.get(x) +"\n";
			}
		}
		
		System.out.println( date2 + "data_retrieval 檢查結果 :" + result +"\n如有餘額對帳錯誤，請確認是否有過去申請的出入金審批通過");
		
	}
	*/
}
