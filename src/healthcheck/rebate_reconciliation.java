package healthcheck;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import dsg_dbhelper.Arith;
import dsg_dbhelper.DB_Helper;

public class rebate_reconciliation {
	

	private rebate_reconciliation()	{}
	
	public static HashMap<String, Object> check() {
		
		HashMap<String,String> closed_trades_tickets = new HashMap<String,String>();
		HashMap<String,String> opening_trades_tickets = new HashMap<String,String>();
		ArrayList<String> rebate_records_tickets = new ArrayList<>();
		HashMap<String,String> lost_tickets = new HashMap<String,String>();
		HashMap<String,String> userids = new HashMap<String,String>();
		TreeSet<String> useridSet = new TreeSet<String>();

		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateStringFormat = new SimpleDateFormat( "yyyy-MM-dd" );
		
		int year = calendar.get(Calendar.YEAR); 
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int week = calendar.get(Calendar.DAY_OF_WEEK);
		String date1 = null;
		String date2 = dateStringFormat.format(calendar.getTime());
		
		//////////////////////
		switch (week) {
		case 2:
			date1 = String.format("%04d-%02d-%02d", year,month,day-3);
			break;

		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
			date1 = String.format("%04d-%02d-%02d", year,month,day-1);
			break;
			
		case 1:
			date1 = String.format("%04d-%02d-%02d", year,month,day-2);
			break;
		}
		
		/////////////////////////////////////////////
		try {
			DB_Helper db = new DB_Helper();
			
			closed_trades_tickets = db.get_closed_trades_ticketids(date1,date2);
			opening_trades_tickets = db.get_opening_trades_ticketids(date1,date2);
			rebate_records_tickets = db.get_rebate_records_ticketids();		
			
		} catch (ClassNotFoundException e) {
			System.out.println("class not found");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL ERROR");
			e.printStackTrace();
		}
		
		String [] ticketsSet1 = new String [closed_trades_tickets.keySet().size()];
		String [] ticketsSet2 = new String [opening_trades_tickets.keySet().size()];
		
		closed_trades_tickets.keySet().toArray(ticketsSet1);
		opening_trades_tickets.keySet().toArray(ticketsSet2);
		
		for(String x : ticketsSet1) {
			
			if(!rebate_records_tickets.contains(x)) {
				
				lost_tickets.put(x, closed_trades_tickets.get(x));
				useridSet.add(closed_trades_tickets.get(x));
			}
			
		}
		
		for(String x : ticketsSet2) {
			
			if(!rebate_records_tickets.contains(x)) {
				
				lost_tickets.put(x, opening_trades_tickets.get(x));
				useridSet.add(opening_trades_tickets.get(x));
			}
		}
		
		String [] lost_ticket = new String[lost_tickets.keySet().size()];
		lost_tickets.keySet().toArray(lost_ticket);
		
		if(!useridSet.isEmpty()) { ////確認每個USERID所擁有有問題的TICKETS

			for(String x : useridSet) {
				String ticket = "";
				for(String y : lost_ticket) {
					if(lost_tickets.get(y).equals(x)) ticket += y+"<br>";
				}

				userids.put(x,ticket);
			}
			
			result.put("note", "交易返佣異常，請確認User返佣設定");

			
		}else {
			result.put("note", "交易返佣正常");
		}
		
		result.put("useridSet", useridSet);
		result.put("userids", userids);
		result.put("lost_ticket", lost_ticket);
		result.put("lost_tickets", lost_tickets);
		
		return result;
		
	}
	
}
